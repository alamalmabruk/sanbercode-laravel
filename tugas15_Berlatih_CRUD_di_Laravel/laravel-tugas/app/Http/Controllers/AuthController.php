<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }

    public function kirim(Request $request){
    $namaDepan = $request->input('fname');
    $namaBelakang = $request->input('lname');

    return view('welcome', ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
    }
    public function table(){
        return view('page.table');
    }
    public function data_tables(){
        return view('page.data_tables');
    }
}
