@extends('layouts.master')

@section('title')
Halaman Detail Cast
@endsection

@section('content')

<h1>{{$cast->nama}}</h1>
<h6>{{$cast->umur}} Years</h6>
<p>{{$cast->bio}}</p>

<a href="/cast" class="btn btn-sm my-3 btn-secondary">Kembali</a>
@endsection