<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class,'home']);
Route::get('/register', [AuthController::class,'register']);
Route::post('/welcome', [AuthController::class,'kirim']);
Route::get('/table',[AuthController::class,'table']);
Route::get('/data-tables',[AuthController::class,'data_tables']);

// Route::get('/master', function(){
// return view('layouts.master');
// });


// CRUD

// -Create Data-
// Route yang mengarahkan ke form tambah data
Route::get('/cast/create',[CastController::class, 'create']);
// Route untuk tambah data ke database
Route::post('/cast', [CastController::class, 'store']);

// -Read Data-
// Route untuk tampil semua Cast
Route::get('/cast',[CastController::class,'index']);
// Route untuk detail category berdasarkan id
Route::get('/cast/{id}',[CastController::class,'show']);

// -Update Data-
// Route untuk mengarah ke form edit data dengan parameter id
Route::get('/cast/{id}/edit', [CastController::class,'edit']);
// Route update data di database berdasarkan id
Route::put('/cast/{id}',[CastController::class,'update']);

// -Delete Data-
Route::Delete('/cast/{id}',[CastController::class,'destroy']);