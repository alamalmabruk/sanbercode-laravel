<?php

require_once('Animal.php');
require_once('Frog.php');
require_once('Ape.php');

$sheep = new Animal("shaun");

echo "Name : ".$sheep->name; // "shaun"
echo "<br>Legs : ".$sheep->legs; // 4
echo "<br>Cold Blooded : ".$sheep->cold_blooded."<br>"; // "no"

$sungokong = new Ape("kera sakti");
echo "<br>Name : ".$sungokong->name; // "shaun"
echo "<br>Legs : ".$sungokong->legs; // 4
echo "<br>Cold Blooded : ".$sungokong->cold_blooded; // "no"
echo "<br>Yell : ".$sungokong->yell()."<br>"; // "Auooo"

$kodok = new Frog("buduk");
echo "<br>Name : ".$kodok->name; // "shaun"
echo "<br>Legs : ".$kodok->legs; // 4
echo "<br>Cold Blooded : ".$kodok->cold_blooded."<br>"; // "no"
echo "Jump : ".$kodok->jump() ; // "hop hop"
?>