<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Genre;
use App\Models\Film;
use File;

class FilmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $film = Film::all();
        return view('film.tampil',['film'=>$film]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genre = Genre::all();
        return view('film.tambah',['genre' => $genre]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required|max:255',
            'poster' => 'required|mimes:jpg,png,jpeg|max:2048'
            //maksimal 2mb = 2048
        ]);
        //mengganti file
        $imageName = time().'.'.$request->poster->extension();

        //tempat penyimpanan file
        $request->poster->move(public_path('image'),$imageName);

        $film = new Film;
        $film->judul = $request->input('judul');
        $film->ringkasan = $request->input('ringkasan');
        $film->tahun = $request->input('tahun');
        $film->poster = $imageName;
        $film->genre_id = $request->input('genre_id');

        $film->save();

        return redirect('/film');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $film = Film::find($id);

        return view('film.detail',['film' => $film]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $film = Film::find($id);
        $genre = Genre::all();

        return view('film.edit',['film' => $film,'genre' => $genre]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required|max:255',
            'poster' => 'required|mimes:jpg,png,jpeg|max:2048'
        ]);

        $film = Film::find($id);

        if($request->has('poster')){
        $path = "image/";
        File::delete($path.$film->poster);
        $imageName = time().'.'.$request->poster->extension();
        $request->poster->move(public_path('image'),$imageName);
        $film->poster = $imageName;
        }

        $film->judul = $request->input('judul');
        $film->ringkasan = $request->input('ringkasan');
        $film->tahun = $request->input('tahun');
        
        $film->genre_id = $request->input('genre_id');

        $film->save();

        return redirect('/film');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $film = Film::find($id);
        $path = "image/";
        File::delete($path . $film->poster);
        $film->delete();
        return redirect('/film');
    }
}
