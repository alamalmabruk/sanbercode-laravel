@extends('layouts.master')

@section('title')
Halaman Tampil Cast
@endsection

@section('content')

<a href="/cast/create" class="btn btn-primary btn-sm my-3">Tambah</a>

<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>

      @forelse ($cast as $key => $item)
        <tr>
            <th scope="row">{{$key + 1}}</th>
            <td>{{ $item->nama }}</td>
            <td>
 
            <form action="/cast/{{ $item->id }}" method="POST">
            @csrf
            @method('delete')
            <a href="/cast/{{ $item->id }}" class="btn btn-sm btn-info">Detail</a>
            <a href="/cast/{{ $item->id }}/edit" class="btn btn-sm btn-warning">Edit</a>
            <input type="submit" value="Delete" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure?')">
          </form>
              </td>
        </tr>
        @empty
            <tr>
                <td> Tidak ada Data Cast</td>
            </tr>
        @endforelse
    </tbody>
  </table>

@endsection