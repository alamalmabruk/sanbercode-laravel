@extends('layouts.master')

@section('title')
Halaman Detail Film
@endsection

@section('content')
<a href="/film" class="btn btn-secondary my-2">Kembali</a>
<img src="{{ asset('image/'.$film->poster) }}" width="100$" height="400px">
<h1 class="text-info">{{ $film->judul }}</h1>
<p>{{ $film->ringkasan }}</p>
@endsection