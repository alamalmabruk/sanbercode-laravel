@extends('layouts.master')

@section('title')
Halaman Tampil Film
@endsection

@section('content')
<div class="row">
    @forelse ($film as $item)
    <div class="col-4">
        <div class="card">
            <img src="{{ asset('image/'.$item->poster) }}" height="300px" class="card-img-top" alt="...">
            <div class="card-body">
              <h5>{{$item->judul}}</h5>
              <p class="card-text">{{ Str::limit($item->ringkasan, 30) }}</p>
              <a href="/film/{{ $item->id }}" class="btn btn-primary btn-block btn-sm">Read More</a>
            <div class="row my-2">
                <div class="col">
                    <a href="/film/{{ $item->id }}/edit" class="btn btn-warning btn-block btn-sm">Edit</a>
                </div>
                <div class="col">
                <form action="/film/{{ $item->id }}" method="POST">
                    @csrf
                    @method('delete')
                    <input type="submit" value="Delete" class="btn btn-danger btn-block btn-sm">
                </form>
            </div>
            </div>
            </div>
          </div>
    </div>        
    @empty
        <h3>Tidak ada berita</h3>
    @endforelse

</div>

@endsection