@extends('layouts.master')

@section('title')
Halaman Tambah Film
@endsection

@section('content')
<form action="/film" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label>Film Genre</label>
       <select class="form-control" name="genre_id" id="">
        <option value="">--Select Genre--</option>
        @forelse ($genre as $item)
            <option value="{{ $item->id }}">{{ $item->nama }}</option>
        @empty
            <option value="">Tidak Ada Genre</option>
        @endforelse
       </select>
      </div>
      @error('judul')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    <div class="form-group">
      <label>Film Title</label>
      <input type="text" name="judul" class="@error('judul') is-invalid @enderror form-control" placeholder="Enter Film Title">
    </div>
    @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Summary</label>
        <textarea class="@error('ringkasan') is-invalid @enderror form-control" name="ringkasan" cols="30" rows="10" placeholder="Enter Film Summary"></textarea>
      </div>
      @error('ringkasan')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Year</label>
        <input type="int" name="tahun" class="@error('tahun') is-invalid @enderror form-control" placeholder="Enter Film Year">
      </div>
      @error('tahun')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Film Poster</label>
        <input type="file" class="form-control" name="poster">
      </div>
      @error('poster')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection