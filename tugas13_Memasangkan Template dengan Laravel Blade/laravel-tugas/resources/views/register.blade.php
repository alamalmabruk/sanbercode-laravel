@extends('layouts.master')

@section('title')
Halaman Pendaftaran
@endsection

@section('content')
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label>First name:</label><br /><br />
        <input type="text" name="fname"/><br /><br>

        <label>Last name:</label><br /><br />
        <input type="text" name="lname"/><br /><br>

        <label>Date of Birth:</label><br>
        <input type="date" name="date_of_birth"><br><br>

        <label>Gender</label><br />
        <input type="radio" name="gender" />Male<br />
        <input type="radio" name="gender" />Female<br />
        <input type="radio" name="gender" />Other<br /><br>

        <label>Nasionality:</label><br>
        <select name="nasionality">
            <option value="indonesian">Indonesian</option>
            <option value="singapoeran">Singaporean</option>
            <option value="malaysian">Malaysian</option>
            <option value="australian">Australian</option>
        </select><br><br>

        <label>Language Spoken:</label><br />
        <input type="checkbox" name="language" />Bahasa Indonesia<br />
        <input type="checkbox" name="language" />English<br />
        <input type="checkbox" name="language" />Other<br /><br>
        <label>Bio:</label><br />
        <textarea cols="30" rows="10"></textarea><br /><br>

        <input type="submit" value="SignUp" />
    </form>
@endsection
